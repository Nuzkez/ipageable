import ioc from "./config/ioc.config";
import SERVICE_IDENTIFIER from "./constants/identifiers.constrant";
import TAG from "./constants/tags.constrant";
import { IPage } from "./interfaces";

let containers = ioc.getAllNamed<IPage>(SERVICE_IDENTIFIER.PAGE, TAG.PAGE);

containers.forEach(page => {
	page.run();
})
