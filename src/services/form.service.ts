import { injectable } from "inversify";
import { IFormService } from "../interfaces";

@injectable()
export class FormService implements IFormService {

	submit(id: string, cb: () => void) {
		let form = <HTMLFormElement>document.getElementById(id);
		form.addEventListener('submit', (e) => {
			e.preventDefault();
			cb();
		})
	}

	reset(id: string){
		let form = <HTMLFormElement>document.getElementById(id);
		form.reset();
	}

	getData<T>(id: string): T {
		let form = <HTMLFormElement>document.getElementById('contact-form');
		let data = new FormData(form);

		let result: T = <T>{};

		data.forEach((value, key, parent) => {
			result[key] = value;
		});

		return result;
	}
}
