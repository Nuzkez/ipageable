import { injectable } from "inversify";
import { IMasked } from "../interfaces";

import Cleave from 'cleave.js';
import 'cleave.js/dist/addons/cleave-phone.KZ';
import { CleaveOptions } from "cleave.js/options";

@injectable()
export class MaskService implements IMasked {

	private readonly phoneOptions: CleaveOptions = {
		phone: true,
		delimiter: '',
		prefix: '+7',
		rawValueTrimPrefix: false,
		onValueChanged: function (e) {
			if (e.target.value.length === 12 && e.target.value.length != 0) {
				this.element.classList.remove("is-invalid");
				this.element.classList.add("is-valid");
			} else {
				this.element.classList.remove("is-valid");
				this.element.classList.add("is-invalid");
			}
		}
	}

	private readonly dateOptions: CleaveOptions = {
		date: true,
		datePattern: ['d', 'm', 'Y'],
		delimiter: '/',
		onValueChanged: function (e) {
			console.log(e.target.value.length)
			if (e.target.value.length === 10 && e.target.value.length != 0) {
				this.element.classList.remove("is-invalid");
				this.element.classList.add("is-valid");
			} else {
				this.element.classList.remove("is-valid");
				this.element.classList.add("is-invalid");
			}
		}
	}

	maskedString(id: string): void {
		let elem = <HTMLInputElement>document.getElementById(id);
		elem.addEventListener('input', function () {
			let length = this.value.length;
			if (length > 2) {
				this.classList.remove("is-invalid");
				this.classList.add("is-valid");
			} else {
				this.classList.remove("is-valid");
				this.classList.add("is-invalid");
			}
		})
	}

	maskedPhoneNumber(id: string, options?: CleaveOptions): void {
		if (options === undefined || options === null) {
			let elem = <HTMLElement>document.getElementById(id);
			new Cleave(elem, this.phoneOptions);
		} else {
			let elem = <HTMLElement>document.getElementById(id);
			new Cleave(elem, options);
		}
	}

	maskedDate(id: string, options?: CleaveOptions): void {
		if (options === undefined || options === null) {
			let elem = <HTMLElement>document.getElementById(id);
			new Cleave(elem, this.dateOptions);
		} else {
			let elem = <HTMLElement>document.getElementById(id);
			new Cleave(elem, options);
		}
	}

}
