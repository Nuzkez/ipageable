import { injectable } from "inversify";
import { User } from "../entities";
import { ITable } from "../interfaces";

@injectable()
export class TableService implements ITable {

	public addUserToTable(user: User): void {
		let tableBody = <HTMLTableElement>document.getElementById('contact-table');
		let tr = document.createElement('tr');

		tr.innerHTML = `            
            <th scope="row">${user.id}</th>
            <td>${user.firstName}</td>
            <td>${user.lastName}</td>
            <td>${user.dateBirth}</td>
            <td>${user.phoneNumber}</td>
            <td>@mdo</td>`

		tableBody.appendChild(tr);
	}
}
