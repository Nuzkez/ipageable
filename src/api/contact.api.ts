import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { injectable } from "inversify";
import { IApi } from "../interfaces";

@injectable()
export class ContactApi implements IApi {

	private readonly _instance: AxiosInstance;

	private readonly config: AxiosRequestConfig = {
		baseURL: 'http://localhost:3000/'
	}

	constructor() {
		this._instance = axios.create(this.config);
	}

	instance(): AxiosInstance {
		return this._instance;
	}
}
