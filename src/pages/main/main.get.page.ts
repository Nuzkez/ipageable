import SERVICE_IDENTIFIER from "../../constants/identifiers.constrant";
import TAG from "../../constants/tags.constrant";

import {
	inject,
	injectable,
	named
} from "inversify";

import { IApi, IPage, ITable } from "../../interfaces";
import { User } from "../../entities";

@injectable()
export class MainGetPage implements IPage {

	@inject(SERVICE_IDENTIFIER.API)
	@named(TAG.CONTACT)
	private readonly contactAPI: IApi;

	@inject(SERVICE_IDENTIFIER.TABLE_SERVICE)
	@named(TAG.TABLE)
	private readonly tableService: ITable;

	private async getContacts(): Promise<void> {
		try {
			const res = await this.contactAPI
				.instance()
				.get<User[]>('contacts')
				.then(value => value.data);

			res.map(user => {
				this.tableService.addUserToTable(user);
			})
		} catch (e) {
			console.log(e.message);
		}
	}


	run(): void {
		this.getContacts();
	}


}
