import {
	inject,
	injectable,
	named
} from "inversify";

import {
	IApi,
	IFormService,
	IPage, ITable
} from "../../interfaces";

import SERVICE_IDENTIFIER from "../../constants/identifiers.constrant";
import TAG from "../../constants/tags.constrant";
import { User } from "../../entities";


@injectable()
export class MainPostPage implements IPage {

	@inject(SERVICE_IDENTIFIER.FORM_SERVICE)
	@named(TAG.FORM)
	private readonly formService: IFormService;

	@inject(SERVICE_IDENTIFIER.API)
	@named(TAG.CONTACT)
	private readonly contactAPI: IApi;

	@inject(SERVICE_IDENTIFIER.TABLE_SERVICE)
	@named(TAG.TABLE)
	private readonly tableService: ITable;

	private readonly formId = 'contact-form';

	isValidate(id: string): boolean {
		let form = <HTMLFormElement>document.getElementById(id);
		let list = form.elements;

		for (let key in list) {
			if (list[key].nodeName === 'INPUT') {
				if (list[key].classList.contains('is-invalid')) {
					return false;
				}
			}
		}

		for (let key in list) {
			if (list[key].nodeName === 'INPUT') {
				list[key].classList.remove('is-valid');
			}
		}

		return true;
	}

	run(): void {
		this.formService.submit(this.formId, async () => {
			if (this.isValidate(this.formId)) {
				let data = this.formService.getData<Partial<User>>(this.formId);

				await this.contactAPI
					.instance()
					.post<User>('contacts', data)
					.then(result => this.tableService.addUserToTable(result.data));

				this.formService.reset(this.formId);
			}
		});
	}
}
