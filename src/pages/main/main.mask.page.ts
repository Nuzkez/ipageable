import { IMasked, IPage } from "../../interfaces";
import { inject, injectable, named } from "inversify";
import SERVICE_IDENTIFIER from "../../constants/identifiers.constrant";
import TAG from "../../constants/tags.constrant";

@injectable()
export class MainMaskPage implements IPage {

	@inject(SERVICE_IDENTIFIER.MASKED_SERVICE)
	@named(TAG.MASK)
	private readonly masked: IMasked


	run(): void {
		this.masked.maskedPhoneNumber('phoneNumber');
		this.masked.maskedDate('dateBirth');
		this.masked.maskedString('firstName');
		this.masked.maskedString('lastName');
	}
}
