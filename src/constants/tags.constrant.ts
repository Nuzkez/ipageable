const TAG = {
	PAGE: 'PAGE',
	FORM: 'FORM',
	MASK: 'MASK',
	CONTACT: 'CONTACT',
	TABLE: 'TABLE'
};

export default TAG;
