const SERVICE_IDENTIFIER = {
	API: Symbol.for('API'),
	PAGE: Symbol.for('PAGE'),
	FORM_SERVICE: Symbol.for('FORM_SERVICE'),
	MASKED_SERVICE: Symbol.for('MASKED_SERVICE'),
	TABLE_SERVICE: Symbol.for('TABLE_SERVICE')
};

export default SERVICE_IDENTIFIER;
