import { AxiosInstance } from "axios";

export interface IApi {
	instance: () => AxiosInstance;
}
