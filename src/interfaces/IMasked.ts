import { CleaveOptions } from "cleave.js/options";

export interface IMasked {
	maskedPhoneNumber(id: string, options?: CleaveOptions): void;
	maskedDate(id: string, options?: CleaveOptions): void;
	maskedString(id: string): void;
}
