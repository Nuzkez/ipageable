export interface IFormService {
	getData<T>(id: string): T;
	submit(id: string, cb: () => void);
	reset(id: string);
}
