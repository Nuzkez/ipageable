import { User } from "../entities";

export interface ITable {
	addUserToTable(user: User): void;
}
