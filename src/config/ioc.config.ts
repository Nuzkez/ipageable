import "reflect-metadata";
import { Container } from "inversify";
import TAG from "../constants/tags.constrant";
import SERVICE_IDENTIFIER from "../constants/identifiers.constrant";

import {
	IFormService,
	IMasked,
	IPage,
	IApi,
	ITable
} from "../interfaces";

import {
	MainMaskPage,
	MainPostPage,
	MainGetPage
} from "../pages";

import {
	FormService,
	MaskService
} from "../services";

import { ContactApi } from "../api/contact.api";
import { TableService } from "../services/table.service";

let ioc = new Container();

//pages
ioc.bind<IPage>(SERVICE_IDENTIFIER.PAGE).to(MainMaskPage).inSingletonScope().whenTargetNamed(TAG.PAGE);
ioc.bind<IPage>(SERVICE_IDENTIFIER.PAGE).to(MainPostPage).inSingletonScope().whenTargetNamed(TAG.PAGE);
ioc.bind<IPage>(SERVICE_IDENTIFIER.PAGE).to(MainGetPage).inSingletonScope().whenTargetNamed(TAG.PAGE);

//entities

//services
ioc.bind<IFormService>(SERVICE_IDENTIFIER.FORM_SERVICE).to(FormService).whenTargetNamed(TAG.FORM);
ioc.bind<IMasked>(SERVICE_IDENTIFIER.MASKED_SERVICE).to(MaskService).whenTargetNamed(TAG.MASK);
ioc.bind<IApi>(SERVICE_IDENTIFIER.API).to(ContactApi).whenTargetNamed(TAG.CONTACT);
ioc.bind<ITable>(SERVICE_IDENTIFIER.TABLE_SERVICE).to(TableService).whenTargetNamed(TAG.TABLE);

export default ioc;
